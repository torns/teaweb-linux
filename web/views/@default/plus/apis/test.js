Tea.context(function () {
    this.selectedTab = "unit";
    this.api = null;
    this.apiPath = "";
    this.selectedMethod = "";
    this.externalURL = "";

    this.headers = [
        {
            "name": "User-Agent",
            "value": navigator.userAgent
        }
    ];

    this.selectTab = function (tab) {
        this.selectedTab = tab;

        if (tab == "benchmark") {
            Tea.activate(Tea.element(".benchmark-box")[0]);
        }

        // 测试计划
        if (tab == "script") {
            Tea.activate(Tea.element(".script-box")[0]);
            this.$delay(function () {
                this.refreshPlans();
            });
        }
    };

    /**
     * 选择要测试的API
     */
    this.apiFrom = "inner";
    this.selectInnerAPI = function () {
        this.apiFrom = "inner";

        // 清除单元测试
        this.testResponse = "";
        this.testResponseStatusCode = 0;
        this.testResponseCost = "";
    };

    this.selectExternalAPI = function () {
        this.apiFrom = "external";
        this.api = {
            "path": "",
            "address": "",
            "params": [],
            "methods": this.externalMethods
        };
        this.selectedMethod = "GET";

        // focus
        this.$delay(function () {
            this.$find(".unit-test-box form input[name='url']").focus();
        });

        // 清除单元测试
        this.testResponse = "";
        this.testResponseStatusCode = 0;
        this.testResponseCost = "";
    };

    this.$delay(function () {
        if (this.apis.length > 0) {
            this.apiPath = this.apis[0].path;
            this.changeApi();
        }
    });

    this.changeApi = function () {
        var that = this;
        this.api = this.apis.$find(function (k, v) {
            return v.path == that.apiPath;
        });

        // 基本信息
        if (this.api != null) {
            this.selectedMethod = this.api.methods[0];
        }

        // 清除单元测试
        this.testResponse = "";
        this.testResponseStatusCode = 0;
        this.testResponseCost = "";
    };

    this.changeMethod = function () {
        // 清除单元测试
        this.testResponse = "";
        this.testResponseStatusCode = 0;
        this.testResponseCost = "";
    };

    this.addHeader = function () {
        this.headers.push({
            "name": "",
            "value": ""
        });
    };

    this.deleteHeader = function (index) {
        this.headers.$remove(index);
    };

    /**
     * 单元测试
     */
    this.testResponse = "";
    this.testResponseHeader = "";
    this.testResponseBody = "";
    this.testResponseStatusCode = 0;
    this.testResponseCost = "";

    // JSON
    var responseJSONEditor = null;

    this.testApiSuccess = function (resp) {
        this.testResponse = resp.data.header + resp.data.body;
        this.testResponseHeader = resp.data.header;
        this.testResponseBody = resp.data.body;
        this.testResponseStatusCode = resp.data.statusCode;
        this.testResponseCost = resp.data.cost;

        this.testShowResponse();
    };

    this.testShowResponse = function () {
        // HTML
        if (this.responseFormat == "html") {
            var htmlFrame = this.$find("#test-response-html");
            if (htmlFrame.length > 0) {
                htmlFrame[0].contentWindow.document.body.innerHTML = this.testResponseBody;
            }
        }

        // JSON
        if (this.responseFormat == "json") {
            if (responseJSONEditor == null) {
                responseJSONEditor = CodeMirror(document.getElementById("test-response-json-editor"), {
                    theme: "idea",
                    lineNumbers: true,
                    styleActiveLine: false,
                    matchBrackets: false,
                    value: "",
                    readOnly: true,
                    height: "auto",
                    viewportMargin: Infinity,
                    scrollbarStyle: null,
                    lineWrapping: true
                });
            }

            try {
                var o = JSON.parse(this.testResponseBody);
                responseJSONEditor.setValue(JSON.stringify(o, null, 4));
            } catch (e) {
                responseJSONEditor.setValue(this.testResponseBody);
            }
        }

        window.location = "#response-result";
    };

    this.testParams = [];

    this.testAddParam = function () {
        this.testParams.push({
            "id": "param_" + Math.random(),
            "name": "",
            "type": "",
            "description": ""
        });
        this.$delay(function () {
            this.$find("#adding-param-input-" + (this.testParams.length - 1)).focus();
        });
    };

    this.testRemoveParam = function (index) {
        this.testParams.$remove(index);
    };

    this.testRemoveInnerParam = function (index) {
        this.api.params.$remove(index);
    };

    this.responseFormat = "source";

    this.switchResponseFormat = function (format) {
        this.responseFormat = format;
        this.$delay(function () {
            this.testShowResponse();
        });
    };

    this.submitUnitTest = function () {
        Tea.runActionOn(this.$find(".unit-test-box form")[0]);
    };

    /** 基准测试 **/
    this.benchmarkLoading = false;

    this.benchmarkReset = function () {
        this.benchmarkAvgCost = "-";
        this.benchmarkTotalCost = "-";
        this.benchmarkTotalBytes = "-";
        this.benchmarkRequestSpeed = "-";
        this.benchmarkBytesSpeed = "-";
        this.benchmarkSuccess = "-";
        this.benchmarkFail = "-";
    };
    this.benchmarkReset();

    this.benchmarkTestBefore = function () {
        this.benchmarkLoading = true;
        this.benchmarkReset();
    };

    this.benchmarkTestSuccess = function (resp) {
        this.benchmarkLoading = false;

        this.benchmarkAvgCost = this.formatDuration(resp.data.avgCost);
        this.benchmarkTotalCost = this.formatDuration(resp.data.totalCost);
        this.benchmarkTotalBytes = this.formatBytes(resp.data.totalBytes);
        this.benchmarkRequestSpeed = Math.ceil(resp.data.requestSpeed) + " requests/s";
        this.benchmarkBytesSpeed = this.formatBytes(resp.data.bytesSpeed) + "/s";
        this.benchmarkSuccess = resp.data.success;
        this.benchmarkFail = resp.data.fail;
    };

    this.benchmarkTestDone = function () {
        this.benchmarkLoading = false;
    };

    this.formatDuration = function (duration) {
        if (duration > 1) {
            return (Math.ceil(duration * 100) / 100) + "s"
        } else {
            return Math.ceil(duration * 1000) + "ms"
        }
    };

    this.formatBytes = function (bytes) {
        bytes = Math.ceil(bytes);
        if (bytes < 1024) {
            return bytes + " bytes";
        }
        if (bytes < 1024 * 1024) {
            return Math.ceil(bytes / 1024) + " k";
        }
        return (Math.ceil(bytes * 100 / 1024 / 1024) / 100) + " m";
    };

    /** 创建脚本 **/
    this.scriptModelVisible = false;

    this.createScriptModel = function () {
        this.scriptModelVisible = true;
        this.editingScript = "";
    };

    this.closeScriptModel = function () {
        this.scriptModelVisible = false;
    };

    /** 自动测试 **/
    this.planAdding = false;
    this.plans = [];
    this.plansLoaded = false;

    this.refreshPlans = function () {
        this.$get(".plans")
            .params({
                "server": this.server.filename
            })
            .success(function (resp) {
                this.plans = resp.data.plans;
            })
            .done(function () {
                this.plansLoaded = true;
            });
    };

    this.addPlan = function () {
        this.planAdding = !this.planAdding;
    };

    this.createPlanSuccess = function () {
        this.refreshPlans();
        this.planAdding = false;
    };

    this.deletePlan = function (filename) {
        if (!window.confirm("确定要删除此计划吗？")) {
            return;
        }
        this.$post(".deletePlan")
            .params({
                "server": this.server.filename,
                "planFilename": filename
            })
            .success(function () {
                this.refreshPlans();
            });
    };

    this.pausePlan = function (plan) {
        this.$post(".pausePlan")
            .params({
                "filename": plan.filename
            })
            .success(function () {
                plan.on = false;
            });
    };

    this.resumePlan = function (plan) {
        this.$post(".resumePlan")
            .params({
                "filename": plan.filename
            })
            .success(function () {
                plan.on = true;
            });
    };

    this.editingPlan = null;
    this.editPlan = function (plan) {
        this.editingPlan = plan;
        this.$delay(function () {
            Tea.activate(Tea.element("#update-plan-box")[0])
        }, 500);
    };

    this.cancelPlanEditing = function () {
        this.editingPlan = null;
    };

    this.updatePlanSuccess = function () {
        this.cancelPlanEditing();
        this.refreshPlans();
    };

    /**
     * 执行计划
     */
    this.runningPlan = null;
    this.runningReport = null;
    this.runningReportFilename = "";
    this.showPlanExecModel = false;

    this.cancelPlanExecuting = function () {
        this.showPlanExecModel = false;

        this.refreshPlans();
    };

    this.execPlan = function (plan) {
        this.runningPlan = plan;
        this.showPlanExecModel = true;

        this.$get(".execPlanStatus")
            .params({
                "filename": plan.filename,
                "reportFilename": this.runningReportFilename
            })
            .success(function (resp) {
                this.runningReport = resp.data.report;
            })
            .fail(function () {
                this.runningReport = null;
            })
            .done(function () {
                if (!this.showPlanExecModel) {
                    return;
                }

                this.$delay(function () {
                    if (!this.showPlanExecModel) {
                        return;
                    }
                    this.execPlan(plan);
                }, 1000);
            });
    };

    this.execPlanStart = function () {
        if (this.runningPlan == null) {
            return;
        }
        this.$post(".execPlan")
            .params({
                "server": this.server.filename,
                "filename": this.runningPlan.filename
            })
            .success(function (resp) {
                this.runningReportFilename = resp.data.reportFilename;
            });
    };

    /**
     * 测试报告
     */
    this.showReportModal = false;
    this.planReports = [];
    this.planSelectedReport = null;
    this.planShowPassedApis = false;
    this.planShowFailedApis = false;
    this.planSelectedApi = null;

    this.showPlanReport = function (plan) {
        this.showReportModal = true;

        this.planReports = [];
        this.planSelectedReport = null;
        this.planShowPassedApis = false;
        this.planShowFailedApis = false;
        this.planSelectedApi = null;

        this.$get(".planReports")
            .params({
                "planFilename": plan.filename
            })
            .success(function (response) {
                this.planReports = response.data.reports;

                if (this.planReports.length > 0) {
                   this.selectPlanReport(this.planReports[0]);
                }
            });
    };

    this.selectPlanReport = function (report) {
        report.info.countPassed = report.info.results.$findAll(function (k, v) {
            return v.isPassed;
        }).length;
        report.info.countFailed = report.info.results.$findAll(function (k, v) {
            return !v.isPassed;
        }).length;
        this.planSelectedReport = report;

        // 选中API
        if (report.info.results.length > 0) {
            this.selectPlanApi(report.info.results[0]);
        }
    };

    this.selectPlanApi = function (apiResult) {
        this.planSelectedApi = null;

        // 获取API信息
        this.$get(".api")
            .params({
                "server": this.server.filename,
                "path": apiResult.api
            })
            .success(function (resp) {
                apiResult.apiName = resp.data.api.name;

                this.planSelectedApi = apiResult;
                this.$delay(function () {
                    this.$find(".report-box .script-code-box")
                        .each(function (k, v) {
                            var editor = CodeMirror(v, {
                                theme: "idea",
                                lineNumbers: false,
                                styleActiveLine: false,
                                matchBrackets: false,
                                value: apiResult.scripts[k].code,
                                readOnly: true,
                                height: "auto",
                                viewportMargin: Infinity,
                                scrollbarStyle: null,
                                lineWrapping: true
                            });
                            editor.refresh();
                        });
                });
            })
            .fail(function () {

            });
    };

    this.cancelReportModal = function () {
        this.showReportModal = false;
    };

    this.selectAllPlanApis = function () {
        this.planShowPassedApis = false;
        this.planShowFailedApis = false;
    };

    this.selectPassedPlanApis = function () {
        this.planShowPassedApis = true;
        this.planShowFailedApis = false;
    };

    this.selectFailedPlanApis = function () {
        this.planShowPassedApis = false;
        this.planShowFailedApis = true;
    };
});
Tea.context(function () {
    this.$delay(function () {
        this.$find("form input[name='path']").focus();
        this.showDescription();
    });

    this.showDescription = function () {
        var editor = CodeMirror.fromTextArea(this.$find("textarea[name='description']")[0], {
            theme: "idea",
            lineNumbers: false,
            styleActiveLine: true,
            matchBrackets: true,
            value: "",
            readOnly: false,
            height: "auto",
            viewportMargin: Infinity,
            lineWrapping: true,
            highlightFormatting: true
        });
        editor.setValue(this.api.description);
        editor.on("change", function () {
            editor.save();
        });
        editor.save();
    };

    var fullDataTypes = this.dataTypes;

   this.focusDataType = function (index, param) {
        param.showDataTypeOptions = true;
        this.changeDataType(index, param);
   };

   this.blurDateType = function (index, param) {
       this.$delay(function () {
           param.showDataTypeOptions = false;
       }, 300);
   };

   this.changeDataType = function (index, param) {
       if (param.type.length == 0) {
           param.showDataTypeOptions = true;
           this.dataTypes = fullDataTypes;
           return;
       }

       var items = fullDataTypes.$filter(function (k, v) {
           return v.code.indexOf(param.type) > -1;
       });
       this.dataTypes = items;
       if (items.length > 0) {
           param.showDataTypeOptions = true;
       } else {
           param.showDataTypeOptions = false;
       }
   };

   this.selectDataType = function (index, param, dataType) {
       param.type = dataType.code;
   };

   this.addingParams = this.api.params.$map(function (k, v) {
       v["id"] = "id_" + Math.random();
       v["showDataTypeOptions"] = false;
       return v;
   });

   this.addParam = function () {
       this.addingParams.push({
           "name": "",
           "description": "",
           "type": "",
           "id": "id_" + Math.random(),
           "showDataTypeOptions": false
       });

       this.$delay(function () {
           var newParam = this.$find("#adding-param-input-" + (this.addingParams.length - 1));
           newParam.focus();
       });
   };

   this.removeParam = function (index) {
       this.addingParams.$remove(index);
   };

   this.afterApiUpdated = function () {
       var newPath = Tea.element("input[name='path']").val();
       window.parent.Tea.Vue.refresh(newPath);
       window.location.reload();
   };

    /**
     * 更多信息
     */
    this.showMoreInfo = false;

    this.showMore = function () {
        this.showMoreInfo = !this.showMoreInfo;
    };

    /**
     * 高级设置
     */
    this.showAdvancedConfig = false;
    this.showAdvanced = function () {
        this.showAdvancedConfig = !this.showAdvancedConfig;
    };
});
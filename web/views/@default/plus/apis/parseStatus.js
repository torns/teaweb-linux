Tea.context(function () {
    var codeEditor;
    var headerEditor;
    var jsonEditor;

    this.$delay(function () {
        codeEditor = CodeMirror.fromTextArea(this.$find("#script-box textarea")[0], {
            theme: "idea",
            lineNumbers: true,
            styleActiveLine: true,
            matchBrackets: true,
            value: "",
            readOnly: false,
            height: "auto",
            viewportMargin: Infinity,
            lineWrapping: true,
            highlightFormatting: true,
            indentUnit: 4,
            mode: "text/javascript",
            indentWithTabs: true
        });

        codeEditor.setValue(this.defaultScript);
        codeEditor.on("change", function () {
            codeEditor.save();
        });


        headerEditor = CodeMirror.fromTextArea(this.$find("#headers-box textarea")[0], {
            theme: "idea",
            lineNumbers: true,
            styleActiveLine: true,
            matchBrackets: true,
            value: "",
            readOnly: false,
            height: "auto",
            viewportMargin: Infinity,
            lineWrapping: true,
            highlightFormatting: true,
            indentUnit: 4,
            mode: "message/http"
        });
        headerEditor.on("change", function () {
            headerEditor.save();
        });

        jsonEditor = CodeMirror.fromTextArea(this.$find("#json-box textarea")[0], {
            theme: "idea",
            lineNumbers: true,
            styleActiveLine: true,
            matchBrackets: true,
            value: "",
            readOnly: false,
            height: "auto",
            viewportMargin: Infinity,
            lineWrapping: true,
            highlightFormatting: true,
            mode: "application/json",
            indentUnit: 4,
            indentWithTabs: true
        });
        jsonEditor.on("change", function () {
            jsonEditor.save();
        });
    });

    this.enableScript = function () {
        this.scriptOn = true;
    };

    this.saveScript = function () {
        this.$post(".parseStatusSave")
            .params({
                "server": this.server.filename,
                "script": codeEditor.getValue(),
                "on": this.scriptOn ? 1 : 0
            })
            .success(function () {
                alert("保存成功");
            });
    };

    this.runScript = function () {
        this.$post(".parseStatusTest")
            .params({
                "script": codeEditor.getValue(),
                "header": headerEditor.getValue(),
                "json": jsonEditor.getValue()
            })
            .success(function (resp) {
                alert("成功，分析出来的状态码为：" + resp.data.status);
            });
    };
});
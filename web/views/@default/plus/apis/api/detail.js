Tea.context(function () {
    this.$delay(function () {
        // link
        Tea.element(".markdown a").each(function (k, v) {
            Tea.element(v).attr("target", "_blank");
        });

        // table
        Tea.element(".markdown table").each(function (k, v) {
            v.className = "ui table";
        });

        // 代码
        Tea.element(".markdown pre code").each(function (k, v) {
            var index = v.className.indexOf("language-");
            if (index < 0) {
                return;
            }
            var element = Tea.element(v);
            var code = element.text();
            code  = code.replace(/\n$/, "");
            element.html("");
            var lang = v.className.substring("language-".length);
            var mimeType = "";
            if (lang == "php") {
                mimeType = "application/x-httpd-php";
            } else if (lang == "javascript") {
                mimeType = "text/javascript";
            } else if (lang == "python") {
                mimeType = "text/x-python";
            } else if (lang == "sh" || lang == "shell") {
                mimeType = "text/x-sh";
            } else if (lang == "http") {
                mimeType = "message/http";
            } else if (lang == "go" || lang == "golang") {
                mimeType = "text/x-go";
            } else if (lang == "node") {
            	mimeType = "text/javascript";
			} else {
                mimeType = "text/x-" + lang;
            }

            var editor = CodeMirror(v, {
                theme: "idea",
                lineNumbers: false,
                value: code,
                readOnly: true,
                height: "auto",
                scrollbarStyle: null,
                viewportMargin: Infinity,
                lineWrapping: true,
                highlightFormatting: true,
                mode: mimeType,
                indentUnit: 4,
                indentWithTabs: true
            });
        });
    });

    this.$delay(function () {
        new ClipboardJS('[data-clipboard-target]');

        this.$find("[data-clipboard-target]").bind("click", function () {
            var em =  Tea.element(this).find("em")[0];
           em.style.cssText = "display:inline";
           setTimeout(function () {
               em.style.cssText = "display:none";
           }, 1000);
        });
    });
});
Tea.context(function () {
    this.$delay(function () {
        var activeItem = this.$find(".main-box .left-box .menu-box .item.active");
        if (activeItem.length > 0) {
            activeItem.focus();
            activeItem.blur();
        }
    });

    // format consumer
    if (this.consumer.policy.traffic.second.duration < 1) {
        this.consumer.policy.traffic.second.duration = 1;
    }
    if (this.consumer.policy.traffic.minute.duration < 1) {
        this.consumer.policy.traffic.minute.duration = 1;
    }
    if (this.consumer.policy.traffic.hour.duration < 1) {
        this.consumer.policy.traffic.hour.duration = 1;
    }
    if (this.consumer.policy.traffic.day.duration < 1) {
        this.consumer.policy.traffic.day.duration = 1;
    }
    if (this.consumer.policy.traffic.month.duration < 1) {
        this.consumer.policy.traffic.month.duration = 1;
    }

    this.apiOn = this.consumer.api.on;
    this.apiAllowAll = this.consumer.api.allowAll;
    this.apiDenyAll = this.consumer.api.denyAll;
    this.apiDenyOpen = this.consumer.api.deny.length > 0;

    this.trafficOn = this.consumer.policy.traffic.on;
    this.trafficTotalOn = this.consumer.policy.traffic.total.on;
    this.trafficSecondOn = this.consumer.policy.traffic.second.on;
    this.trafficMinuteOn = this.consumer.policy.traffic.minute.on;
    this.trafficHourOn = this.consumer.policy.traffic.hour.on;
    this.trafficDayOn = this.consumer.policy.traffic.day.on;
    this.trafficMonthOn = this.consumer.policy.traffic.month.on;

    this.accessOn = this.consumer.policy.access.on;
    this.accessAllowOn = this.consumer.policy.access.allowOn;
    this.accessDenyOn = this.consumer.policy.access.denyOn;
    this.accessAllowIPs = this.consumer.policy.access.allow;
    this.accessDenyIPs = this.consumer.policy.access.deny;

    var allowApis = this.allowApis;
    var denyApis = this.denyApis;

    this.checkAPI = function (api) {
        api.isChecked = !api.isChecked;
    };

    this.countAllowApis = function () {
        return allowApis.$findAll(function (k, v) {
            return v.isChecked;
        }).length;
    };

    this.cancelAllAllowApis = function () {
        allowApis.$each(function (k, v) {
            v.isChecked = false;
        });
    };

    this.countDenyApis = function () {
        return denyApis.$findAll(function (k, v) {
            return v.isChecked;
        }).length;
    };

    this.cancelAllDenyApis = function () {
        denyApis.$each(function (k, v) {
            v.isChecked = false;
        });
    };

    this.addAccessAllowIP = function () {
        this.accessAllowIPs.push("");
    };

    this.removeAccessAllowIP = function (index) {
        this.accessAllowIPs.$remove(index);
    };

    this.addAccessDenyIP = function () {
        this.accessDenyIPs.push("");
    };

    this.removeAccessDenyIP = function (index) {
        this.accessDenyIPs.$remove(index);
    };

    this.deleteConsumer = function (serverFilename, consumerFilename) {
        if (!window.confirm("确定要删除此认证用户吗？")) {
            return;
        }
        this.$post("/plus/apis/auth/consumer/delete")
            .params({
                "server": serverFilename,
                "consumerFilename": consumerFilename
            })
            .success(function () {
                Tea.go(".", { "server": serverFilename });
            });
    };
});
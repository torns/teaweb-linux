Tea.context(function () {
    this.funcs = [];
    this.isChanged = false;

    var editor;

    this.$delay(function () {
        var that = this;

        editor = CodeMirror(document.getElementById("code-editor"), {
            theme: "idea",
            lineNumbers: true,
            styleActiveLine: true,
            matchBrackets: true,
            value: that.code,
            height: "auto",
            viewportMargin: Infinity,
            scrollbarStyle: null,
            lineWrapping: true,
            indentUnit: 4,
            indentWithTabs: true
        });

        var funcs = [
            [ "API", 'apis.API("API路径")' ],
            [ "method", "method(\"请求方法\")" ],
            [ "param", 'param("参数名", "参数值")' ],
            [ "body", 'body("要上传的内容")'],
            [ "repeat", "repeat(重复次数)" ],
            [ "timeout", "timeout(秒数)" ],
            [ "concurrent", "concurrent(并发数)" ],
            [ "header", "header(\"名称\", \"值\")" ],
            [ "remoteAddr", "remoteAddr(\"客户端地址\")" ],
            [ "domain", "domain(\"域名\")" ],
            [ "file", "file(\"参数名\", \"文件路径\")" ],
            [ "cookie", "cookie(\"名称\", \"值\")" ],
            [ "author", "author(\"脚本作者\")" ],
            [ "description", "description(\"脚本描述\")" ],
            [ "assertFormat", "assertFormat(\"格式\")" ],
            [ "assertHeader", "assertHeader(\"名称\", \"值\", \"错误消息\")" ],
            [ "assertStatus", "assertStatus(状态码, \"错误消息\")" ],
            [ "assert", "assert(\"字段\", function (value) {\n\treturn true;\n}, \"错误消息\")" ],
            [ "assertEqual", "assertEqual(\"字段\", \"值\", \"错误消息\")" ],
            [ "assertNotEqual", "assertNotEqual(\"字段\", \"值\", \"错误消息\")" ],
            [ "assertGt", "assertGt(\"字段\", 数值, \"错误消息\")" ],
            [ "assertGte", "assertGte(\"字段\", 数值, \"错误消息\")" ],
            [ "assertLt", "assertLt(\"字段\", 数值, \"错误消息\")" ],
            [ "assertLte", "assertLte(\"字段\", 数值, \"错误消息\")" ],
            [ "assertTrue", "assertTrue(\"字段\", \"错误消息\")" ],
            [ "assertFalse", "assertFalse(\"字段\", \"错误消息\")" ],
            [ "assertLength", "assertLength(\"字段\", 长度数值, \"错误消息\")" ],
            [ "assertNotEmpty", "assertNotEmpty(\"字段\", \"错误消息\")" ],
            [ "assertEmpty", "assertEmpty(\"字段\", \"错误消息\")" ],
            [ "assertExist", "assertExist(\"字段\", \"错误消息\"\")" ],
            [ "assertType", "assertType(\"字段\", \"类型\", \"错误消息\")" ],
            [ "assertBool", "assertBool(\"字段\", \"错误消息\")" ],
            [ "assertNumber", "assertNumber(\"字段\", \"错误消息\")" ],
            [ "assertString", "assertString(\"字段\", \"错误消息\")" ],
            [ "assertInt", "assertInt(\"字段\", \"错误消息\")" ],
            [ "assertFloat", "assertFloat(\"字段\", \"错误消息\")" ],
            [ "assertArray", "assertArray(\"字段\", \"错误消息\")" ],
            [ "assertObject", "assertObject(\"字段\", \"错误消息\")" ],
            [ "assertNull", "assertNull(\"字段\", \"错误消息\")" ],
            [ "assertExist", "assertExist(\"字段\", \"错误消息\")" ],
            [ "assertNotExist", "assertNotExist(\"字段\", \"错误消息\")" ],
            [ "addFailure", "addFailure(\"错误消息\")" ],
            [ "onSuccess", "onSuccess(function (resp) {\n\n})" ],
            [ "onError", "onError(function (resp) {\n\n})" ],
            [ "onDone", "onDone(function (resp) {\n\n})" ]
        ];

        editor.on("keyup", function (doc) {
            var hintBox = Tea.element("#code-editor .hint");

            var cursor = doc.getCursor();

            // 查找最近的token
            if (cursor.ch == 0) {
                hintBox[0].style.cssText = "display:none;";
                return;
            }
            var keyword = "";
            var fromCh = -1;
            for (var i = cursor.ch - 1; i >= 0; i --) {
                var token = doc.getRange({
                    line: cursor.line,
                    ch: i
                }, {
                    line: cursor.line,
                    ch: cursor.ch
                });
                if (!token.match(/^\w+$/)) {
                    break;
                }
                fromCh = i;
                keyword = token;
            }

            // 判断是否有正确的keyword
            if (keyword.length == 0) {
                hintBox[0].style.cssText = "display:none;";
                return;
            }

            // 查找函数
            that.funcs = [];
            funcs.$each(function (k, v) {
                if (v[0].toLowerCase().indexOf(keyword.toLowerCase()) > -1) {
                    that.funcs.push({
                        from: { line: cursor.line, ch: fromCh },
                        to: cursor,
                        f: v
                    });
                }
            });
            that.funcs = that.funcs.slice(0, 10);

            if (that.funcs.length == 0) {
                hintBox[0].style.cssText = "display:none;";
                return;
            }


            var bound = doc.charCoords({
                line: cursor.line,
                ch: fromCh
            });

            hintBox[0].style.cssText = "display:block;left:" + (bound.left) + "px;top:" + (bound.top + 20) + "px;";
        });

        editor.on("change", function () {
            that.isChanged = true;
        });
    });

    this.selectFunc = function (func) {
        editor.replaceRange(func.f[1] + "", func.from, func.to);

        var hintBox = Tea.element("#code-editor .hint");
        hintBox[0].style.cssText = "display:none;";
    };

    this.closeHint = function () {
        var hintBox = Tea.element("#code-editor .hint");
        hintBox[0].style.cssText = "display:none;";
    };

    /**
     * 执行测试
     */
    this.exec = function () {
        var code = editor.getValue();
        this.$post("/plus/apis/api/script/exec")
            .params({
                "server": this.server.filename,
                "path": this.path,
                "code": code
            })
            .success(function () {

            });
    };

    /**
     * 保存
     */
    this.save = function () {
        var code = editor.getValue();
        this.$post("/plus/apis/api/script/save")
            .params({
                "server": this.server.filename,
                "path": this.path,
                "script": this.script,
                "code": code
            })
            .success(function () {
                window.parent.Tea.Vue.refreshScripts();
                window.parent.Tea.Vue.closeScriptModel();
            });
    };
});
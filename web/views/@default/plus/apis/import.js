Tea.context(function () {
    this.apis = [];

    this.checkAPI = function (api) {
        api.isChecked = !api.isChecked;
    };

    this.importUploadSuccess = function (resp) {
       this.apis = resp.data.apis;
    };
});
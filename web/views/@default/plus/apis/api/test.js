Tea.context(function () {
    this.selectedTab = "unit";
    this.selectedMethod = this.api.methods[0];

    this.selectUnitTest = function () {
        this.selectedTab = "unit";
    };

    this.selectBenchmarkTest = function () {
        this.selectedTab = "benchmark";
        this.$delay(function () {
            Tea.activate(Tea.element(".benchmark-box")[0])
        });
    };

    this.selectScriptTest = function () {
        this.selectedTab = "script";
        this.refreshScripts();
    };

    this.selectMethod = function (method) {

    };

    /** 单元测试 **/
    this.testResponse = "";
    this.testResponseHeader = "";
    this.testResponseBody = "";
    this.testResponseStatusCode = 0;
    this.testResponseCost = "";

    // JSON
    var responseJSONEditor = null;

    this.testApiSuccess = function (resp) {
        this.testResponse = resp.data.header + resp.data.body;
        this.testResponseHeader = resp.data.header;
        this.testResponseBody = resp.data.body;
        this.testResponseStatusCode = resp.data.statusCode;
        this.testResponseCost = resp.data.cost;

        this.testShowResponse();
    };

    this.testShowResponse = function () {
        // HTML
        if (this.responseFormat == "html") {
            var htmlFrame = this.$find("#test-response-html");
            if (htmlFrame.length > 0) {
                htmlFrame[0].contentWindow.document.body.innerHTML = this.testResponseBody;
            }
        }

        // JSON
        if (this.responseFormat == "json") {
            if (responseJSONEditor == null) {
                var editorBox = document.getElementById("test-response-json-editor");
                editorBox.style.cssText = "width:" + (window.innerWidth - 20) + "px";
                responseJSONEditor = CodeMirror(editorBox, {
                    theme: "idea",
                    lineNumbers: true,
                    styleActiveLine: false,
                    matchBrackets: false,
                    value: "",
                    readOnly: true,
                    height: "auto",
                    viewportMargin: Infinity,
                    scrollbarStyle: null,
                    lineWrapping: true
                });
            }

            try {
                var o = JSON.parse(this.testResponseBody);
                responseJSONEditor.setValue(JSON.stringify(o, null, 4));
            } catch (e) {
                responseJSONEditor.setValue(this.testResponseBody);
            }
        }

        window.location = "#response-result";
    };

    this.headers = [
        {
            "name": "User-Agent",
            "value": navigator.userAgent
        }
    ];

    this.addHeader = function () {
        this.headers.push({
            "name": "",
            "value": ""
        });
    };

    this.deleteHeader = function (index) {
        this.headers.$remove(index);
    };

    this.testParams = [];

    this.testAddParam = function () {
        this.testParams.push({
            "id": "param_" + Math.random(),
            "name": "",
            "type": "",
            "description": ""
        });

        this.$delay(function () {
            this.$find("#adding-param-input-" + (this.testParams.length - 1)).each(function (k, v) {
                v.focus();
            });
        });
    };

    this.testRemoveParam = function (index) {
        this.testParams.$remove(index);
    };

    this.testRemoveInnerParam = function (index) {
        this.api.params.$remove(index);
    };

    this.responseFormat = "source";

    this.switchResponseFormat = function (format) {
        this.responseFormat = format;
        this.$delay(function () {
            this.testShowResponse();
        });
    };

    this.submitUnitTest = function () {
        Tea.runActionOn(this.$find(".unit-test-box form")[0]);
    };

    this.savingUnitTest = null;
    this.casesVisible = false;
    this.usingUnitTest = null;
    this.selectedCase = null;

    this.saveUnitTest = function () {
        var data = {
            "name": "未命名",
            "domain": Tea.element(".unit-test-box form *[name='domain']").val(),
            "method": Tea.element(".unit-test-box form *[name='method']").val(),
            "query": Tea.element(".unit-test-box form *[name='query']").val(),
            "headers": this.headers,
            "params": this.api.params,
            "attachParams": this.testParams,
            "format": this.responseFormat
        };

        for (var i = 0; i < data.params.length; i++) {
            data.params[i].value = Tea.element(".unit-test-box form *[name='value_" + data.params[i].name + "']").val();
        }
        for (var i = 0; i < data.attachParams.length; i++) {
            data.attachParams[i].value = Tea.element(".unit-test-box form *[name='value_" + data.attachParams[i].name + "']").val();
        }

        this.savingUnitTest = data;
    };

    this.closeUnitTestSaving = function () {
        this.savingUnitTest = null;
    };

    this.saveUnitTestConfirm = function () {
        if (this.savingUnitTest == null) {
            alert("操作失败，请刷新后重试");
            return;
        }

        var json = JSON.stringify(this.savingUnitTest);
        this.$post(".saveUnitTest")
            .params({
                "apiFilename": this.api.filename,
                "caseFilename": this.savingUnitTest.filename,
                "content": json
            })
            .success(function () {
                alert("保存成功");
                this.closeUnitTestSaving();

                this.refreshCases();
            });
    };

    this.selectSavingCase = function (case1) {
        this.savingUnitTest.filename = case1.filename;
        this.savingUnitTest.name = case1.name;
    };

    this.createNewCase = function () {
        this.savingUnitTest.filename = null;
        this.savingUnitTest.name = "未命名";
    };

    this.updateCase = function (case1) {
        var json = JSON.stringify(case1);
        this.$post(".saveUnitTest")
            .params({
                "apiFilename": this.api.filename,
                "caseFilename": case1.filename,
                "content": json
            })
            .success(function () {
                alert("保存成功");
                this.refreshCases();
            });
    };

    this.showCases = function () {
        this.casesVisible = true;
        if (this.selectedCase == null) {
            if (this.cases.length > 0) {
                this.selectedCase = this.cases[0];
            }
        } else {
            var that = this;
            this.cases.$each(function (k, v) {
                if (v.filename == that.selectedCase.filename) {
                    that.selectedCase = v;
                }
            });
        }
    };

    /**
     * 使用用例参数
     */
    this.useCase = function (case1) {
        this.usingUnitTest = case1;

        var form = Tea.element(".unit-test-box");
        this.domain = case1.domain;
        this.selectedMethod = case1.method;
        form.find("*[name='query']").val(case1.query);
        this.headers = case1.headers;

        var that = this;

        this.api.params = [];

        for (var i = 0; i < case1.params.length; i ++) {
            this.api.params.push(case1.params[i]);
            (function (i) {
                that.$delay(function () {
                    form.find("*[name='value_" + case1.params[i].name + "']").val(case1.params[i].value);
                });
            })(i);
        }

        this.testParams = [];
        for (var i = 0; i < case1.attachParams.length; i ++) {
            this.testParams.push(case1.attachParams[i]);
            (function (i) {
                that.$delay(function () {
                    form.find("*[name='value_" + case1.attachParams[i].name + "']").val(case1.attachParams[i].value);
                });
            })(i);
        }

        if (case1.format != null && case1.format.length > 0) {
            this.switchResponseFormat(case1.format);
        }

        this.casesVisible = false;
        window.location = "#response-result";
    };

    /**
     * 直接执行此用例
     */
    this.execCase = function (case1) {
        this.useCase(case1);
        this.$delay(function () {
            this.submitUnitTest();
        });
    };

    this.refreshCases = function () {
        // 刷新
        this.$get(".testCases")
            .params({
                "apiFilename": this.api.filename
            })
            .success(function (resp) {
                this.cases = resp.data.cases;
                if (this.selectedCase == null && this.cases.length > 0) {
                    this.selectedCase = this.cases[0];
                }
            });
    };

    this.selectCase = function (case1) {
        this.selectedCase = case1;
    };

    this.deleteCase = function (case1) {
        if (!window.confirm("确定要删除此测试用例吗？")) {
            return;
        }
        this.$post(".deleteTestCase")
            .params({
                "apiFilename": this.api.filename,
                "caseFilename": case1.filename
            })
            .success(function () {
                this.selectedCase = null;
                this.refreshCases();
            });
    };

    /** 基准测试 **/
    this.benchmarkLoading = false;

    this.benchmarkReset = function () {
        this.benchmarkAvgCost = "-";
        this.benchmarkTotalCost = "-";
        this.benchmarkTotalBytes = "-";
        this.benchmarkRequestSpeed = "-";
        this.benchmarkBytesSpeed = "-";
        this.benchmarkSuccess = "-";
        this.benchmarkFail = "-";
    };
    this.benchmarkReset();

    this.benchmarkTestBefore = function () {
        this.benchmarkLoading = true;
        this.benchmarkReset();
    };

    this.benchmarkTestSuccess = function (resp) {
        this.benchmarkLoading = false;

        this.benchmarkAvgCost = this.formatDuration(resp.data.avgCost);
        this.benchmarkTotalCost = this.formatDuration(resp.data.totalCost);
        this.benchmarkTotalBytes = this.formatBytes(resp.data.totalBytes);
        this.benchmarkRequestSpeed = Math.ceil(resp.data.requestSpeed) + " requests/s";
        this.benchmarkBytesSpeed = this.formatBytes(resp.data.bytesSpeed) + "/s";
        this.benchmarkSuccess = resp.data.success;
        this.benchmarkFail = resp.data.fail;
    };

    this.benchmarkTestDone = function () {
        this.benchmarkLoading = false;
    };

    this.formatDuration = function (duration) {
        if (duration > 1) {
            return (Math.ceil(duration * 100) / 100) + "s"
        } else {
            return Math.ceil(duration * 1000) + "ms"
        }
    };

    this.formatBytes = function (bytes) {
        bytes = Math.ceil(bytes);
        if (bytes < 1024) {
            return bytes + " bytes";
        }
        if (bytes < 1024 * 1024) {
            return Math.ceil(bytes / 1024) + " k";
        }
        return (Math.ceil(bytes * 100 / 1024 / 1024) / 100) + " m";
    };

    /** 创建脚本 **/
    this.scriptModelVisible = false;

    this.createScriptModel = function () {
        this.scriptModelVisible = true;
        this.editingScript = "";
    };

    this.closeScriptModel = function () {
        this.scriptModelVisible = false;
    };

    /**
     * 刷新脚本
     */
    this.scripts = [];
    this.scriptsLoaded = false;
    this.refreshScripts = function () {
        this.scripts = [];

        this.$get(".scripts")
            .params({
                "server": this.server.filename,
                "path": this.selectedApiPath
            })
            .success(function (resp) {
                var scripts = resp.data.scripts;
                this.scripts = scripts;

                this.$delay(function () {
                    this.$find(".script-box")
                        .each(function (index, box) {
                            var index = Tea.element(box).attr("index");
                            if (index.length == 0) {
                                return;
                            }
                            index = parseInt(index);
                            CodeMirror(box, {
                                theme: "idea",
                                lineNumbers: false,
                                styleActiveLine: false,
                                matchBrackets: false,
                                value: scripts[index].code,
                                readOnly: true,
                                height: "auto",
                                viewportMargin: Infinity,
                                scrollbarStyle: null,
                                lineWrapping: true
                            });
                        });
                });
            })
            .done(function () {
                this.scriptsLoaded = true;
            });
    };

    this.deleteScript = function (filename) {
        if (!window.confirm("确定要删除此脚本吗？")) {
            return;
        }
        this.$post(".script.delete")
            .params({
                "server": this.server.filename,
                "path": this.selectedApiPath,
                "script": filename
            })
            .success(function () {
                this.refreshScripts();
            });
    };

    this.execScript = function (code) {
        this.$post("/plus/apis/api/script/exec")
            .params({
                "server": this.server.filename,
                "path": this.selectedApiPath,
                "code": code
            })
            .success(function () {

            });
    };

    /**
     * 修改脚本
     */
    this.editingScript = "";

    this.editScript = function (script) {
        this.scriptModelVisible = true;
        this.editingScript = script.filename;
    };
});
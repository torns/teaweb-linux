Tea.context(function () {
    this.showDataTypeOptions = false;

    this.$delay(function () {
        this.$find("form input[name='path']").focus();
        this.initMarkdown();
    });

    var fullDataTypes = this.dataTypes;

   this.focusDataType = function (param) {
        this.changeDataType(param);
   };

   this.blurDateType = function (param) {
       this.$delay(function () {
           param.showDataTypeOptions = false;
       }, 300);
   };

   this.changeDataType = function (param) {
       if (param.type.length == 0) {
           param.showDataTypeOptions = true;
           this.dataTypes = fullDataTypes;
           return;
       }

       var items = fullDataTypes.$filter(function (k, v) {
           return v.code.indexOf(param.type) > -1;
       });
       this.dataTypes = items;
       if (items.length > 0) {
           param.showDataTypeOptions = true;
       } else {
           param.showDataTypeOptions = false;
       }
   };

   this.selectDataType = function (param, dataType) {
       param.type = dataType.code;
   };

   this.addingParams = [];

   this.addParam = function () {
       this.addingParams.push({
           "id": "id_" + Math.random(),
           "type": "",
           "showDataTypeOptions": false
       });
       this.$delay(function () {
            var newParam = this.$find("#adding-param-input-" + (this.addingParams.length - 1));
            newParam.focus();
       });
   };

   this.removeParam = function (index) {
       this.addingParams.$remove(index);
   };

   this.initMarkdown = function () {
       this.$find("form textarea[name='description']")
           .each(function (k, v) {
               var editor = CodeMirror.fromTextArea(v, {
                   theme: "idea",
                   lineNumbers: false,
                   styleActiveLine: true,
                   matchBrackets: true,
                   value: "",
                   readOnly: false,
                   height: "auto",
                   viewportMargin: Infinity,
                   lineWrapping: true,
                   highlightFormatting: true
               });

               editor.on("change", function () {
                   editor.save();
               });
           });
   };

    /**
     * 更多信息
     */
    this.showMoreInfo = false;

    this.showMore = function () {
        this.showMoreInfo = !this.showMoreInfo;
    };

    /**
     * 高级设置
     */
    this.showAdvancedConfig = false;
    this.showAdvanced = function () {
        this.showAdvancedConfig = !this.showAdvancedConfig;
    };
});
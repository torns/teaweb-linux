Tea.context(function () {
    /**
     * 监控
     */
    this.apiLogs = [];
    this.apiSelectedLog = null;
    this.apiLoaded = false;
    this.apiInspectTab = "response";
    this.apiLogFromId = "";

    this.$delay(function () {
        this.watchApi();
    });

    this.watchApi = function () {
        this.$post(".watch")
            .params({
                "server": this.server.filename,
                "path": this.selectedApiPath,
                "fromId": this.apiLogFromId
            })
            .success(function (resp) {
                if (resp.data.logs.length == 0) {
                    return;
                }
                this.apiLogFromId = resp.data.logs.$first().id;
                this.apiLogs = resp.data.logs.$filter(function (k, v) {
                    return v.requestData.length > 0;
                }).concat(this.apiLogs);

                if (this.apiSelectedLog == null && this.apiLogs.length > 0) {
                    this.selectApiLog(this.apiLogs.$first());
                }
            })
            .done(function () {
                this.apiLoaded = true;

                this.$delay(function () {
                    this.watchApi();
                }, 1000);
            });
    };

    this.formatCost = function (seconds) {
        var s = (seconds * 1000).toString();
        var pieces = s.split(".");
        if (pieces.length < 2) {
            return s;
        }

        return pieces[0] + "." + pieces[1].substr(0, 3);
    };

    this.selectApiLog = function (log) {
        this.apiSelectedLog = log;
        this.$delay(function() {
            if (this.apiInspectTab == "response") {
                this.inspectApiResponse();
            } else if (this.apiInspectTab == "request") {
                this.inspectApiRequest();
            }
        });
    };

    var requestEditor = null;
    var responseEditor = null;

    this.inspectApiResponse = function () {
        this.apiInspectTab = "response";
        var codeBox = this.$find(".bottom-pane.response code");
        if (responseEditor == null) {
            responseEditor = CodeMirror(codeBox[0], {
                theme: "idea",
                lineNumbers: false,
                styleActiveLine: false,
                matchBrackets: true,
                value: this.apiSelectedLog.responseHeaderData + this.apiSelectedLog.responseBodyData,
                readOnly: true,
                height: "auto",
                scrollbarStyle: null,
                viewportMargin: Infinity,
                lineWrapping: true,
                highlightFormatting: true,
                mode: "message/http",
                indentUnit: 4,
                indentWithTabs: true
            })
        } else {
            responseEditor.setValue(this.apiSelectedLog.responseHeaderData + this.apiSelectedLog.responseBodyData);
        }
    };

    this.inspectApiRequest = function () {
        this.apiInspectTab = "request";
        this.$delay(function () {
            var codeBox = this.$find(".bottom-pane.request code");

            if (requestEditor == null) {
                requestEditor = CodeMirror(codeBox[0], {
                    theme: "idea",
                    lineNumbers: false,
                    styleActiveLine: false,
                    matchBrackets: true,
                    value: this.apiSelectedLog.requestData,
                    readOnly: true,
                    height: "auto",
                    scrollbarStyle: null,
                    viewportMargin: Infinity,
                    lineWrapping: true,
                    highlightFormatting: true,
                    mode: "message/http",
                    indentUnit: 4,
                    indentWithTabs: true
                })
            } else {
                requestEditor.setValue(this.apiSelectedLog.requestData);
            }
        });
    };
});
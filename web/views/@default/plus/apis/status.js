Tea.context(function () {
    this.statusAdding = false;
    this.addingType = "success";

    this.filterKeyword = "";
    this.filterGroup = "";
    this.filterVersion = "";
    this.filterType = "";
    this.isFiltering = false;

    var allStatusList = this.statusList;

    this.hasStatusList = allStatusList.length > 0;

    this.$delay(function () {
        if (this.$find("#status-list-table").length == 0) {
            return;
        }
        var table = this.$find("#status-list-table")[0];
        var that = this;
        Sortable.create(table, {
            draggable: "tbody",
            onStart: function () {
                table.className = "ui table";
            },
            onUpdate: function (event) {
                var newIndex = event.newIndex;
                var oldIndex = event.oldIndex;
                that.$post(".moveStatus")
                    .params({
                        "server": that.server.filename,
                        "oldIndex": oldIndex,
                        "newIndex": newIndex
                    })
                    .success(function () {
                        window.location.reload();
                    });
            }
        });
    }, 1000);


    this.filter = function () {
        this.isFiltering = true;

        var that = this;
        this.statusList = allStatusList.$filter(function (k, v) {
            var b1 = (that.filterGroup.length > 0) ? v.groups.$contains(that.filterGroup) : true;
            var b2 = (that.filterVersion.length > 0) ? v.versions.$contains(that.filterVersion): true;
            var b3 = (that.filterType.length > 0) ? v.type == that.filterType : true;
            var b4 = (teaweb.match(v.code, that.filterKeyword)
                || teaweb.match(v.type, that.filterKeyword)
                || teaweb.match(v.typeName, that.filterKeyword)
                || teaweb.match(v.description, that.filterKeyword)
                || teaweb.match(v.groups.join(" "), that.filterKeyword)
                || teaweb.match(v.versions.join(" "), that.filterKeyword));

            return b1 && b2 && b3 && b4;
        });
    };

    this.clearFilters = function () {
        this.filterKeyword = "";
        this.filterGroup = "";
        this.filterVersion = "";
        this.filterType = "";
        this.filter();

        this.isFiltering = false;
    };

    this.addStatus = function () {
        this.statusAdding = !this.statusAdding;
        if (this.statusAdding) {
            this.$delay(function () {
                this.$find("#add-status-form input[name='code']").focus();
            });
        }
    };

    this.deleteStatus = function (status) {
        if (!window.confirm("确定要删除此状态码吗？")) {
            return;
        }
        this.$post(".deleteStatus")
            .params({
                "server": this.server.filename,
                "code": status.code
            });
    };

    this.statusEditing = null;
    this.editStatus = function (status) {
        this.statusEditing = JSON.parse(JSON.stringify(status));
        this.$delay(function () {
            window.location = "#status-code-editing";
        });
    };

    this.cancelEditing = function () {
        this.statusEditing = null;
    };

    this.updateStatus = function (status) {
        var newName = this.$find("#status-form input[name='editingName']").val().trim();
        this.$post(".updateStatus")
            .params({
                "server": this.server.filename,
                "oldName": status,
                "newName": newName
            });
    };

    this.moveDownStatus = function (status) {
        this.$post(".moveDownStatus")
            .params({
                "server": this.server.filename,
                "code": status.code
            });
    };

    this.moveUpStatus = function (status) {
        this.$post(".moveUpStatus")
            .params({
                "server": this.server.filename,
                "code": status.code
            });
    };
});
Tea.context(function () {
   this.$delay(function () {
       var activeItem = this.$find(".main-box .left-box .menu-box .item.active");
       if (activeItem.length > 0) {
           activeItem.focus();
           activeItem.blur();
       }
   });

   this.deleteConsumer = function (serverFilename, consumerFilename) {
        if (!window.confirm("确定要删除此认证用户吗？")) {
            return;
        }
        this.$post("/plus/apis/auth/consumer/delete")
            .params({
                "server": serverFilename,
                "consumerFilename": consumerFilename
            })
            .success(function () {
                Tea.go(".", { "server": serverFilename });
            });
   };
});
Tea.context(function () {
    var that = this;

    this.firstLoaded = true;
    this.mocks = [];
    this.selectedFormat = "json";
    this.headers = [];
    this.showAddingForm = false;

    this.stat = {};

    this.$delay(function () {
        this.refreshMocks();
    });

    this.changeMockStatus = function () {
        this.$post(".updateMockStatus")
            .params({
                "server": this.server.filename,
                "apiFilename": this.apiFilename,
                "status": this.api.mockOn ? 1 : 0
            })
            .success(function (resp) {
                window.parent.Tea.Vue.updateMockStatus(this.apiFilename, resp.data.mockOn);
            })
            .fail(function () {
                this.api.mockOn = !this.api.mockOn;
            });
    };

    this.selectFormat = function (format) {
        this.selectedFormat = format;
        this.showAddingForm = false;
        this.refreshMocks();
    };

    this.addMock = function () {
        if (this.showAddingForm) {
            this.showAddingForm = false;
            return;
        }
        this.showAddingForm = true;

        var text = "";
        if (this.selectedFormat == "json") {
            this.headers = [{
                "name": "Content-Type",
                "value": "application/json; charset=utf-8"
            }];
            text = "{\n\t\n}";
        } else if (this.selectedFormat == "xml") {
            this.headers = [{
                "name": "Content-Type",
                "value": "text/xml; charset=utf-8"
            }];
        } else if (this.selectedFormat == "text") {
            this.headers = [{
                "name": "Content-Type",
                "value": "text/plain; charset=utf-8"
            }];
        } else {
            this.headers = [{
                "name": "Content-Type",
                "value": ""
            }];
        }

        this.$delay(function () {
            // 初始化
            Tea.activate(document.getElementById("adding-form-box"));

            // 清除以前
            this.$find("#adding-form-box .CodeMirror").remove();

            if (this.selectedFormat == "json") {
                var editor = CodeMirror.fromTextArea(document.getElementById("json-mock-box"), {
                    theme: "idea",
                    lineNumbers: true,
                    styleActiveLine: true,
                    matchBrackets: true,
                    value: "",
                    readOnly: false,
                    height: "auto",
                    viewportMargin: Infinity,
                    scrollbarStyle: null,
                    lineWrapping: true,
                    mode: "application/json",
                    indentUnit: 4,
                    indentWithTabs: true
                });

                editor.setValue(text);
                editor.focus();

                editor.on("change", function () {
                    editor.save();
                });
            }
            else  if (this.selectedFormat == "xml") {
                var editor = CodeMirror.fromTextArea(document.getElementById("xml-mock-box"), {
                    theme: "idea",
                    lineNumbers: true,
                    styleActiveLine: true,
                    matchBrackets: true,
                    value: "",
                    readOnly: false,
                    height: "auto",
                    viewportMargin: Infinity,
                    scrollbarStyle: null,
                    lineWrapping: true,
                    mode: "text/xml",
                    indentUnit: 4,
                    indentWithTabs: true
                });

                editor.setValue(text);
                editor.focus();

                editor.on("change", function () {
                    editor.save();
                });
            }
            else if (this.selectedFormat == "text") {
                this.$find("#adding-form-box textarea[name='text']").focus();
            }
        });
    };

    this.addHeader = function () {
        this.headers.push({
            "name": "",
            "value": ""
        });
    };

    this.deleteHeader = function (index) {
        this.headers.$remove(index);
    };

    this.createMockSuccess = function () {
        alert("添加成功");
        this.showAddingForm = false;
        this.refreshMocks();
    };

    this.refreshMocks = function () {
        this.mocks = [];

        this.$get(".mocks")
            .params({
                "apiFilename": this.apiFilename
            })
            .success(function (resp) {
                this.mocks = resp.data.mocks;
                this.stat = {};
                var firstFormat = null;
                this.mocks.$each(function (k, v) {
                    if (firstFormat == null) {
                        firstFormat = v.format;
                    }
                    if (typeof(that.stat[v.format]) == "undefined") {
                        that.stat[v.format] = 1;
                    } else {
                        that.stat[v.format] ++;
                    }
                });

                // 自动跳转到有数据的format
                if (this.firstLoaded && (this.stat[this.selectedFormat] == null || this.stat[this.selectedFormat] == 0) && firstFormat != null) {
                    this.selectedFormat = firstFormat;
                }
                this.firstLoaded = false;

                this.$delay(function () {
                    var boxes = this.$find(".mock-text-box");
                    boxes.each(function (k, box) {
                        if (that.selectedFormat == "json") {
                            var editor = CodeMirror.fromTextArea(box, {
                                theme: "idea",
                                lineNumbers: true,
                                styleActiveLine: true,
                                matchBrackets: true,
                                value: "",
                                readOnly: false,
                                height: "auto",
                                viewportMargin: Infinity,
                                scrollbarStyle: null,
                                lineWrapping: true,
                                mode: "application/json",
                                indentUnit: 4,
                                indentWithTabs: true
                            });

                            editor.on("change", function () {
                                var index = parseInt(Tea.element(box).attr("data-index"));
                                that.mocks[index].text = editor.getValue();
                                that.mocks[index].isChanged = true;
                                Vue.set(that.mocks, index, that.mocks[index]);
                            });
                        }
                        else if (that.selectedFormat == "xml") {
                            var editor = CodeMirror.fromTextArea(box, {
                                theme: "idea",
                                lineNumbers: true,
                                styleActiveLine: true,
                                matchBrackets: true,
                                value: "",
                                readOnly: false,
                                height: "auto",
                                viewportMargin: Infinity,
                                scrollbarStyle: null,
                                lineWrapping: true,
                                mode: "text/xml",
                                indentUnit: 4,
                                indentWithTabs: true
                            });

                            editor.on("change", function () {
                                var index = parseInt(Tea.element(box).attr("data-index"));
                                that.mocks[index].text = editor.getValue();
                                that.mocks[index].isChanged = true;
                                Vue.set(that.mocks, index, that.mocks[index]);
                            });
                        }
                        else if (that.selectedFormat == "text") {
                            Tea.element(".mock-text-box").bind("input", function () {
                                var box = this;
                                var index = parseInt(Tea.element(box).attr("data-index"));
                                that.mocks[index].text = box.value;
                                that.mocks[index].isChanged = true;
                                Vue.set(that.mocks, index, that.mocks[index]);
                            });
                        }
                    });
                });
            });
    };

    this.deleteMock = function (mock) {
        if (!window.confirm("确定要删除此Mock吗？")) {
            return;
        }
        this.$post(".deleteMock")
            .params({
                "server": this.server.filename,
                "apiFilename": this.apiFilename,
                "mockFilename": mock.filename
            })
            .success(function () {
                this.refreshMocks();
            });
    };

    this.saveMock = function (mock, index) {
        this.$post(".updateMockText")
            .params({
                "mockFilename": mock.filename,
                "text": mock.text
            })
            .success(function () {
                alert("保存成功");
                mock.isChanged = false;
                this.$set(this.mocks, index, mock);
            });
    };
});
Tea.context(function () {
   this.disableUser = function (username) {
        this.$post(".disableUser")
            .params({
                "username": username
            });
   };

   this.enableUser = function (username) {
       this.$post(".enableUser")
           .params({
               "username": username
           });
   };
});
Tea.context(function () {
    this.checkAPI = function (api) {
        api.isChecked = !api.isChecked;
    };

    this.selectAll = function () {
        this.apis.$each(function (k, api) {
            api.isChecked = true;
        });
    };

   this.deselectAll = function () {
       this.apis.$each(function (k, api) {
            api.isChecked = false;
       });
   };

   this.beforeExport = function () {
       this.exportFile = "";

       var countChecked = this.apis.$findAll(function (k, api) {
            return api.isChecked;
       }).length;
       if (countChecked == 0) {
           alert("请选择要导出的API");
           return false;
       }
       if (!window.confirm("确定要导出选中的" + countChecked + "个API吗？")) {
           return false;
       }
   };

   this.exportFile = "";
   this.successExport = function (resp) {
        this.exportFile = resp.data.tarName;
   };

   this.closeExportMessageBox = function () {
       this.exportFile = "";
   };
});
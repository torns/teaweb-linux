Tea.context(function () {
    this.groupAdding = false;

    this.addGroup = function () {
        this.groupAdding = !this.groupAdding;
        if (this.groupAdding) {
            this.$delay(function () {
                this.$find("#add-group-form input[name='name']").focus();
            });
        }
    };

    this.deleteGroup = function (groupName) {
        if (!window.confirm("确定要删除此分组吗？")) {
            return;
        }
        this.$post(".deleteGroup")
            .params({
                "server": this.server.filename,
                "name": groupName
            });
    };

    this.editingGroup = "";
    this.editGroup = function (groupName) {
        this.editingGroup = groupName;
    };

    this.cancelGroupEditing = function () {
        this.editingGroup = "";
    };

    this.updateGroup = function (group) {
        var newName = this.$find("#groups-form input[name='editingName']").val().trim();
        this.$post(".updateGroup")
            .params({
                "server": this.server.filename,
                "oldName": group,
                "newName": newName
            });
    };

    this.moveDownGroup = function (group) {
        this.$post(".moveDownGroup")
            .params({
                "server": this.server.filename,
                "name": group
            });
    };

    this.moveUpGroup = function (group) {
        this.$post(".moveUpGroup")
            .params({
                "server": this.server.filename,
                "name": group
            });
    };
});
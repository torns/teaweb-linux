Tea.context(function () {
    var allApis = this.apis;

    this.apiKeyword = "";
    this.selectedGroup = "all";
    this.selectedApi = null;
    this.apiLoading = false;

    this.$delay(function () {
        if (this.selectedApiPath.length > 0) {
            this.queryApi(this.selectedApiPath);

            // 滚动到具体位置
            this.$find(".api-list .menu .item.active").focus();
        }
    });

    this.searchApi = function (keyword) {
        var that = this;
        if (keyword.length == 0) {
            this.apis = [];
            this.apis.$pushAll(allApis.$filter(function (k, v) {
                if (that.selectedApiGroups.length > 0) {
                    if (v.groups == null) {
                        return false;
                    }

                    if (that.selectedApiGroups.$intersect(v.groups).length == 0){
                        return false;
                    }
                }

                if (that.selectedApiVersions.length > 0) {
                    if (v.versions == null) {
                        return false;
                    }

                    if (that.selectedApiVersions.$intersect(v.versions).length == 0){
                        return false;
                    }
                }

                return true;
            }));
        } else {
            this.apis = allApis.$filter(function (k, v) {
                if (that.selectedApiGroups.length > 0) {
                    if (v.groups == null) {
                        return false;
                    }

                    if (that.selectedApiGroups.$intersect(v.groups).length == 0){
                        return false;
                    }
                }

                return teaweb.match(v.name, keyword)
                    || teaweb.match(v.path, keyword)
                    || teaweb.match(v.address, keyword)
                    || teaweb.match(v.username, keyword)
                    || teaweb.match(v.userName, keyword)
                    || v.methods.$any(function (k, v) {
                        return teaweb.match(v, keyword);
                    })
                    || v.groups.$any(function (k, v) {
                        return teaweb.match(v, keyword);
                    })
                    || v.versions.$any(function (k, v) {
                        return teaweb.match(v, keyword);
                    })
                    || (v.todo.length > 0 && teaweb.match("todo", keyword))
                    || (v.done.length > 0 && teaweb.match("done", keyword))
                    || (v.isDeprecated && teaweb.match("过期", keyword))
                    || (v.isDeprecated && keyword == "deprecated")
                    || (!v.on && teaweb.match("禁用", keyword))
                    || (v.hasTestScripts && teaweb.match("script", keyword))
                    || (v.hasMocks && teaweb.match("mock", keyword))
                    || (v.authType.length > 0 && v.authType != 'none' && teaweb.match(v.authTypeName, keyword));
            });
        }

        // 排序
        if (this.selectedGroup == "modified") {
            this.apis.$rsort(function (v1, v2) {
                return v1.modifiedAt - v2.modifiedAt;
            });
        }
    };

    this.refresh = function (selectedPath) {
        this.selectedApiPath = selectedPath;
        this.$get(".all")
            .params({
                "server": this.server.filename
            })
            .success(function (response) {
                allApis = response.data.apis;
                this.searchApi(this.apiKeyword);
            });

    };

    this.removeApiKeyword = function () {
        this.apiKeyword = "";
        this.searchApi("");
    };

    this.selectGroup = function (group) {
        this.selectedGroup = group;
        this.searchApi(this.apiKeyword);

        this.$find(".api-list")[0].scrollTop = 0;

        if (group == "group") {
            this.showApiGroups = true;
        }

        if (group == "version") {
            this.showApiVersions = true;
        }
    };

    this.selectApi = function (api) {
        this.selectedApiPath = api.path;
        this.queryApi(this.selectedApiPath);
    };

    this.queryApi = function (path) {
        this.apiLoading = true;
        this.$get("/plus/apis/api")
            .params({
                "server": this.server.filename,
                "path": path
            })
            .success(function (resp) {
                this.selectedApi = resp.data.api;
            })
            .done(function () {
                this.apiLoading = false;
            });
    };

    /**
     * 分组
     */
    this.selectedApiGroups = [];
    this.showApiGroups = false;

    this.selectApiGroup = function (group) {
        if (this.selectedApiGroups.$contains(group)) {
            this.selectedApiGroups.$removeValue(group);
        } else {
            //this.selectedApiGroups.push(group);
            this.selectedApiGroups = [ group ];
        }
        this.searchApi(this.apiKeyword);
        this.showApiGroups = false;
    };

    this.closeApiGroups = function () {
        this.showApiGroups = !this.showApiGroups;
    };

    this.resetSelectedApiGroups = function () {
        this.selectedApiGroups = [];
        this.searchApi(this.apiKeyword);
    };

    /**
     * 版本
     */
    this.selectedApiVersions = [];
    this.showApiVersions = true;

    this.selectApiVersion = function (version) {
        if (this.selectedApiVersions.$contains(version)) {
            this.selectedApiVersions.$removeValue(version);
        } else {
            //this.selectedApiVersions.push(version);
            this.selectedApiVersions = [ version ];
        }
        this.searchApi(this.apiKeyword);
        this.showApiVersions = false;
    };

    this.closeApiVersions = function () {
        this.showApiVersions = !this.showApiVersions;
    };

    this.resetSelectedApiVersions = function () {
        this.selectedApiVersions = [];
        this.searchApi(this.apiKeyword);
    };

    /**
     * 刷新当前Tab
     */
    this.refreshApiTab = function () {
        var iframe = this.$find(".api-box-right iframe");
        if (iframe.length > 0) {
            iframe[0].contentWindow.location.reload();
        }
    };

    /**
     * 详情
     */
    this.selectedApiTab = "detail";

    this.selectApiDetail = function () {
        this.selectedApiTab = "detail";
        this.queryApi(this.selectedApiPath);
    };

    this.selectApiModify = function () {
        this.selectedApiTab = "modify";
    };

    this.selectApiWatch = function () {
        this.selectedApiTab = "watch";
    };

    this.selectApiStat = function () {
        this.selectedApiTab = "stat";
    };

    /** 假数据 **/
    this.selectApiMock = function () {
        this.selectedApiTab = "mock";
    };

    this.testApi = function () {
        this.selectedApiTab = "test";
    };

    this.deleteApi = function (path) {
        if (!window.confirm("确定要删除此API吗？")) {
            return;
        }

        this.selectedApi = null;
        this.$get("/plus/apis/api/delete")
            .params({
                "server": this.server.filename,
                "path": path
            })
            .success(function () {
                if (this.selectedApiPath == path) {
                    this.selectedApiPath = "";
                    for (var i = 0; i < this.apis.length; i ++) {
                        if (this.apis[i].path == path) {
                            continue;
                        }
                        this.selectedApiPath = this.apis[i].path;
                        this.selectApiDetail();
                        break;
                    }
                }

                this.refresh(this.selectedApiPath);
            });
    };

    /**
     * Mock
     */
    this.updateMockStatus = function (filename, status) {
        allApis.$find(function (k, v) {
            return v.filename == filename;
        }).hasMocks = status;
    };
});
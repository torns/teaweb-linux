Tea.context(function () {
    this.topRequests = [];
    this.topCosts = [];

    /**
     * 开发者用户统计
     */
    this.todoUsers = [];
    this.doneUsers = [];
    for (var username in this.userStat) {
        var stat = this.userStat[username];
        stat["username"] = username
        if (stat["todoApis"].length > 0) {
            this.todoUsers.push(stat);
        }
        if (stat["doneApis"].length > 0) {
            this.doneUsers.push(stat);
        }
    }
    this.todoUsers.$sort(function (v1, v2) {
        return (v2.todoApis.length - v1.todoApis.length) ;
    });
    this.doneUsers.$sort(function (v1, v2) {
        return (v2.doneApis.length - v1.doneApis.length) ;
    });

    // 请求统计
    this.timeRange = "hourly";

    this.selectTimeRange = function (range) {
        this.timeRange = range;
        this.loadChart();
    };

    this.$delay(function () {
        this.loadChart();
    });

    this.loadChart = function () {
        this.$get(".boardData")
            .params({
                "serverId": this.server.id,
                "range": this.timeRange
            })
            .success(function (response) {
                // top requests
                this.topRequests = response.data.topRequests.$map(function (k, v) {
                    v.percent = Math.ceil(v.percent * 10000) / 100;
                    return v;
                });

                // top costs
                if (this.topCosts.length > 0) {
                    var max = this.topCosts[0].avgCost;
                    if (max < 0.01) {
                        max = 0.01;
                    }
                    this.topCosts = response.data.topCosts.$map(function (k, v) {
                        v.percent = Math.ceil(v.avgCost * 100 * 100 / max) / 100;
                        v.avgCost = Math.ceil(v.avgCost * 1000 * 100) / 100;
                        return v;
                    });
                }

                // 趋势
                this.chartTitle = response.data.title;
                var chart = echarts.init(this.$find(".board .chart-box")[0]);

                // 指定图表的配置项和数据
                var option = {
                    textStyle: {
                        fontFamily: "Lato,'Helvetica Neue',Arial,Helvetica,sans-serif"
                    },
                    title: {
                        text: "",
                        top: 0,
                        x: "center"
                    },
                    tooltip: {},
                    legend: {
                        data: []
                    },
                    xAxis: {
                        data: response.data.labels
                    },
                    yAxis: {
                        axisLabel: {
                            formatter: function (value) {
                                if (value < 10000) {
                                    return value;
                                }
                                return (Math.round(value * 100 / 10000) / 100) + "万"
                            }
                        }
                    },
                    series: [{
                        name: '',
                        type: 'line',
                        data: response.data.data,
                        areaStyle: {
                            color: "#2185d0",
                            opacity: 0.8
                        },
                        lineStyle: {
                            color: "rgba(0, 0, 0, 0)"
                        },
                        itemStyle: {
                            color: "#2185d0",
                            opacity: 0
                        }
                    }],
                    grid: {
                        left: 50,
                        right: 50,
                        bottom: 30,
                        top: 10
                    },
                    axisPointer: {
                        show: true
                    },
                    tooltip: {
                        formatter: 'X:{b0} Y:{c0}'
                    }
                };

                chart.setOption(option);
            })
            .done(function () {
                this.$delay(function () {
                    this.loadChart();
                }, 10000);
            })
            .error(function () {

            });
    };
});
Tea.context(function () {
    this.timeRange = "hourly";

    this.$delay(function () {
        this.loadChart();
    }, 100);


    this.selectTimeRange = function (range) {
        this.timeRange = range;
        this.loadChart();
    };

    this.loadChart = function () {
        // 检查宽度
        var chartBox = this.$find("#chart-box")[0];
        if (chartBox.offsetWidth == 0) {
            this.$delay(function () {
                this.loadChart();
            }, 100);
            return;
        }

        this.$post(".stat")
            .params({
                "server": this.server.filename,
                "path": this.selectedApiPath,
                "range": this.timeRange
            })
            .success(function (response) {
                var chart = echarts.init(chartBox);

                // 指定图表的配置项和数据
                var option = {
                    textStyle: {
                        fontFamily: "Lato,'Helvetica Neue',Arial,Helvetica,sans-serif"
                    },
                    title: {
                        text: "",
                        top: 0,
                        x: "center"
                    },
                    tooltip: {},
                    legend: {
                        data: []
                    },
                    xAxis: {
                        data: response.data.labels
                    },
                    yAxis: {
                        axisLabel: {
                            formatter: function (value) {
                                if (value < 10000) {
                                    return value;
                                }
                                return (Math.round(value * 100 / 10000) / 100) + "万"
                            }
                        }
                    },
                    series: [{
                        name: '',
                        type: 'line',
                        data: response.data.data,
                        areaStyle: {
                            color: "#2185d0",
                            opacity: 0.8
                        },
                        lineStyle: {
                            color: "rgba(0, 0, 0, 0)"
                        },
                        itemStyle: {
                            color: "#2185d0",
                            opacity: 0
                        }
                    }],
                    grid: {
                        left: 50,
                        right: 50,
                        bottom: 30,
                        top: 10
                    },
                    axisPointer: {
                        show: true
                    },
                    tooltip: {
                        formatter: 'X:{b0} Y:{c0}'
                    }
                };

                chart.setOption(option);
            });
    };
});
Tea.context(function () {
    this.menuMatch = function () {
        var arr = [];
        for (var i = 0; i < arguments.length; i ++) {
            arr.push(arguments[i]);
        }
        return arr.$contains(window.location.pathname);
    };

    this.menuStarts = function () {
        var arr = [];
        for (var i = 0; i < arguments.length; i ++) {
            if (window.location.pathname.indexOf(arguments[i]) > -1) {
                return true;
            }
        }
        return false;
    };
});
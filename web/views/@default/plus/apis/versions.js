Tea.context(function () {
    this.versionAdding = false;

    this.addVersion = function () {
        this.versionAdding = !this.versionAdding;
        if (this.versionAdding) {
            this.$delay(function () {
                this.$find("#add-version-form input[name='name']").focus();
            });
        }
    };

    this.deleteVersion = function (versionName) {
        if (!window.confirm("确定要删除此分组吗？")) {
            return;
        }
        this.$post(".deleteVersion")
            .params({
                "server": this.server.filename,
                "name": versionName
            });
    };

    this.editingVersion = "";
    this.editVersion = function (versionName) {
        this.editingVersion = versionName;
    };

    this.cancelVersionEditing = function () {
        this.editingVersion = "";
    };

    this.updateVersion = function (version) {
        var newName = this.$find("#versions-form input[name='editingName']").val().trim();
        this.$post(".updateVersion")
            .params({
                "server": this.server.filename,
                "oldName": version,
                "newName": newName
            });
    };

    this.moveDownVersion = function (version) {
        this.$post(".moveDownVersion")
            .params({
                "server": this.server.filename,
                "name": version
            });
    };

    this.moveUpVersion = function (version) {
        this.$post(".moveUpVersion")
            .params({
                "server": this.server.filename,
                "name": version
            });
    };
});